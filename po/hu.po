# Hungrian translation for ClamTk, 5.25
# Copyright (C) Dave M < dave.nerd @ gmail >, 2004-2017.
# This file is distributed under the same license as the ClamTk package.
# Németh Tamás <ntomasz@vipmail.hu>, 2009.
# Muszela Balázs <bazsi86@gmail.com>, 2009, 2010.
# kontosman, 2010.
# Gergely Szarka <szarka.honved@gmail.com>, 2010.
# Papp Bence <papp.bence89@gmail.com>, 2010.
# Gabor Kelemen <kelemeng@openscope.org>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: ClamTk-5.25\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-11-11 02:42-0600\n"
"PO-Revision-Date: 2017-07-17 11:22+0000\n"
"Last-Translator: Gabor Kelemen <kelemeng@openscope.org>\n"
"Language-Team: Hungarian <NONE>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2017-08-26 15:48+0000\n"
"X-Generator: Launchpad (build 18446)\n"

#: lib/Analysis.pm:49 lib/GUI.pm:525 lib/GUI.pm:608 lib/GUI.pm:749
#: lib/Results.pm:136 lib/Results.pm:209
msgid "Analysis"
msgstr "Vizsgálat"

#: lib/Analysis.pm:72
msgid "File Analysis"
msgstr "Fájl vizsgálata"

#: lib/Analysis.pm:79 lib/Results.pm:35
msgid "Results"
msgstr "Eredmények"

#: lib/Analysis.pm:143
msgid "Vendor"
msgstr "Gyártó"

#: lib/Analysis.pm:152
msgid "Date"
msgstr "Dátum"

#: lib/Analysis.pm:161
msgid "Result"
msgstr "Eredmény"

#: lib/Analysis.pm:180
msgid "Save results"
msgstr "Eredmények mentése"

#: lib/Analysis.pm:196
msgid "Check or recheck a file's reputation"
msgstr "Fájl hírnevének ellenőrzése vagy újraellenőrzése"

#: lib/Analysis.pm:213 lib/GUI.pm:648 lib/Shortcuts.pm:40
msgid "Select a file"
msgstr "Válasszon egy fájlt"

#: lib/Analysis.pm:247
msgid "Submit file for analysis"
msgstr "Fájl elemzésre küldése"

#: lib/Analysis.pm:268
msgid "Uploaded files must be smaller than 32MB"
msgstr "A feltöltendő fájlnak kisebbnek kell lennie mint 32MB"

#: lib/Analysis.pm:287
msgid ""
"No information exists for this file. Press OK to submit this file for "
"analysis."
msgstr ""
"Nincs információ erről a fájlról. Nyomja meg az OK gombot a fájl "
"elküldéséhez elemzésre."

#: lib/Analysis.pm:298 lib/Analysis.pm:618
msgid "Unable to submit file: try again later"
msgstr "Nem sikerült elküldeni a fájlt: Próbálja újra később."

#: lib/Analysis.pm:310
msgid "View or delete previous results"
msgstr "Előző elemzési eredmények megtekintése vagy törlése"

#: lib/Analysis.pm:336
msgid "View file results"
msgstr "Elemzési eredmények megtekintése"

#: lib/Analysis.pm:398
msgid "Delete file results"
msgstr "Elemzési eredmények törlése"

#: lib/Analysis.pm:471 lib/Analysis.pm:572 lib/Assistant.pm:103
#: lib/Update.pm:156
msgid "Please wait..."
msgstr "Kis türelmet…"

#: lib/Analysis.pm:616
msgid "File successfully submitted for analysis."
msgstr "Sikeresen beküldte a fájlt elemzésre."

#: lib/Analysis.pm:676 lib/Analysis.pm:681
msgid "Unable to save file"
msgstr "A fájl nem menthető"

#: lib/Analysis.pm:690
msgid "File has been saved"
msgstr "A fájl elmentve"

#: lib/App.pm:245
msgid "Unknown"
msgstr "Ismeretlen"

#: lib/App.pm:396 lib/Prefs.pm:172
msgid "Never"
msgstr "Soha"

#: lib/App.pm:454 clamtk-gnome.py:58
msgid "Scan for threats..."
msgstr "Vírusok keresése..."

#: lib/App.pm:455
msgid "Advanced"
msgstr "Speciális"

#: lib/App.pm:456 lib/GUI.pm:499 lib/GUI.pm:500 lib/GUI.pm:611
msgid "Scan a file"
msgstr "Egy fájl vizsgálata"

#: lib/App.pm:457 lib/GUI.pm:504 lib/GUI.pm:505 lib/GUI.pm:614
msgid "Scan a directory"
msgstr "Könyvtár vizsgálata"

#: lib/App.pm:459
msgid "ClamTk is a graphical front-end for Clam Antivirus"
msgstr "A ClamTk egy grafikus felület a Clam Antivirushoz"

#: lib/Assistant.pm:37
msgid "Please choose how you will update your antivirus signatures"
msgstr "Válassza ki, hogyan szeretné frissíteni az adatbázist"

#: lib/Assistant.pm:55
msgid "My computer automatically receives updates"
msgstr "A számítógépem automatikusan kapjon frissítéseket"

#: lib/Assistant.pm:70
msgid "I would like to update signatures myself"
msgstr "Manuálisan szeretném frissíteni az adatbázist"

#: lib/Assistant.pm:96 lib/Assistant.pm:154
msgid "Press Apply to save changes"
msgstr "Kattintson az Alkalmazás gombra a módosítások mentéséhez"

#: lib/Assistant.pm:126
msgid "Your changes were saved."
msgstr "A módosítások elmentve."

#: lib/Assistant.pm:129 lib/Update.pm:175 lib/Update.pm:181
msgid "Error updating: try again later"
msgstr "Sikertelen frissítés: próbálja újra később"

#: lib/GUI.pm:54
msgid "Virus Scanner"
msgstr "Víruskereső"

#: lib/GUI.pm:87
msgid "ClamTk Virus Scanner"
msgstr "ClamTk víruskereső"

#: lib/GUI.pm:99 lib/GUI.pm:605
msgid "Help"
msgstr "Súgó"

#: lib/GUI.pm:105 lib/GUI.pm:602 lib/Shortcuts.pm:28
msgid "About"
msgstr "Névjegy"

#: lib/GUI.pm:113
msgid "Quit"
msgstr "Kilépés"

#: lib/GUI.pm:160
msgid "Updates are available"
msgstr "Frissítések érhetők el"

#: lib/GUI.pm:163
msgid "The antivirus signatures are outdated"
msgstr "Az antivírus szabályok elavultak"

#: lib/GUI.pm:166
msgid "An update is available"
msgstr "Frissítés érhető el"

#: lib/GUI.pm:264 lib/GUI.pm:627
msgid "Settings"
msgstr "Beállítások"

#: lib/GUI.pm:265
msgid "View and set your preferences"
msgstr "Személyes beállítások megtekintése és módosítása"

#: lib/GUI.pm:269 lib/GUI.pm:635
msgid "Whitelist"
msgstr "Fehérlista"

#: lib/GUI.pm:270
msgid "View or update scanning whitelist"
msgstr "Fehérlista megtekintése vagy frissítése"

#: lib/GUI.pm:274 lib/GUI.pm:633
msgid "Network"
msgstr "Hálózat"

#: lib/GUI.pm:275
msgid "Edit proxy settings"
msgstr "Proxy beállítások módosítása"

#: lib/GUI.pm:279 lib/GUI.pm:617
msgid "Scheduler"
msgstr "Ütemező"

#: lib/GUI.pm:280
msgid "Schedule a scan or signature update"
msgstr "Vizsgálat vagy adatbázis-frissítés ütemezés"

#: lib/GUI.pm:349 lib/GUI.pm:629
msgid "Update"
msgstr "Frissítés"

#: lib/GUI.pm:350
msgid "Update antivirus signatures"
msgstr "Adatbázis frissítése"

#: lib/GUI.pm:354 lib/GUI.pm:637
msgid "Update Assistant"
msgstr "Frissítésvarázsló"

#: lib/GUI.pm:355
msgid "Signature update preferences"
msgstr "Adatbázis-frissítési beállítások"

#: lib/GUI.pm:423 lib/GUI.pm:625 lib/GUI.pm:737 lib/History.pm:38
msgid "History"
msgstr "Előzmények"

#: lib/GUI.pm:424
msgid "View previous scans"
msgstr "Előző vizsgálatok megtekintése"

#: lib/GUI.pm:428 lib/GUI.pm:631 lib/Results.pm:121 lib/Results.pm:194
msgid "Quarantine"
msgstr "Karantén"

#: lib/GUI.pm:429
msgid "Manage quarantined files"
msgstr "Karanténba zárt elemek kezelése"

#: lib/GUI.pm:526
msgid "View a file's reputation"
msgstr "Fájl hírnevének megjelenítése"

#: lib/GUI.pm:671 lib/GUI.pm:709 lib/Scan.pm:740 lib/Scan.pm:751
msgid "You do not have permissions to scan that file or directory"
msgstr "Nincs jogosultsága a fájl vagy könyvtár vizsgálatához"

#: lib/GUI.pm:685 lib/Shortcuts.pm:46 lib/Whitelist.pm:96
msgid "Select a directory"
msgstr "Válasszon egy könyvtárat"

#: lib/GUI.pm:730
msgid "Configuration"
msgstr "Beállítások"

#: lib/GUI.pm:743
msgid "Updates"
msgstr "Frissítések"

#: lib/GUI.pm:811
msgid "Please install yelp to view documentation"
msgstr "Telepítse a \"yelp\"-et a dokumentáció megjelenítéséhez"

#: lib/GUI.pm:837
msgid "Homepage"
msgstr "Honlap"

#: lib/History.pm:74
msgid "View"
msgstr "Nézet"

#: lib/History.pm:144
#, perl-format
msgid "Viewing %s"
msgstr "%s megjelenítése"

#: lib/History.pm:160
#, perl-format
msgid "Problems opening %s..."
msgstr "Hiba %s megnyitásakor..."

#: lib/Network.pm:48
msgid "No proxy"
msgstr "Nincs proxy"

#: lib/Network.pm:54
msgid "Environment settings"
msgstr "Környezet beállításai"

#: lib/Network.pm:60
msgid "Set manually"
msgstr "Beállítás kézzel"

#: lib/Network.pm:65
msgid "IP address or host"
msgstr "IP-cím vagy gépnév"

#: lib/Network.pm:90
msgid "Port"
msgstr "Port"

#: lib/Network.pm:271
msgid "Settings saved"
msgstr "Beállítások elmentve"

#: lib/Network.pm:275
msgid "Error"
msgstr "Hiba"

#: lib/Quarantine.pm:53
msgid "Number"
msgstr "Szám"

#: lib/Quarantine.pm:61 lib/Results.pm:68
msgid "File"
msgstr "Fájl"

#: lib/Quarantine.pm:74
msgid "Restore"
msgstr "Visszaállítás"

#: lib/Quarantine.pm:110 lib/Results.pm:259
#, perl-format
msgid "Really delete this file (%s) ?"
msgstr "Valóban törli ezt a fájlt (%s) ?"

#: lib/Quarantine.pm:171
msgid "Save file as..."
msgstr "Fájl mentése másként…"

#: lib/Results.pm:79 lib/Schedule.pm:187
msgid "Status"
msgstr "Állapot"

#: lib/Results.pm:90
msgid "Action Taken"
msgstr "Felismert művelet"

#: lib/Results.pm:127 lib/Results.pm:202
msgid "Delete"
msgstr "Törlés"

#: lib/Results.pm:148
msgid "Close"
msgstr "Bezárás"

#: lib/Results.pm:197
msgid "Quarantined"
msgstr "Karanténba helyezve"

#: lib/Results.pm:205
msgid "Deleted"
msgstr "Törölve"

#: lib/Scan.pm:134
msgid "Preparing..."
msgstr "Előkészítés…"

#: lib/Scan.pm:145 lib/Scan.pm:380
#, perl-format
msgid "Files scanned: %d"
msgstr "Ellenőrzött fájlok: %d"

#: lib/Scan.pm:149 lib/Scan.pm:450
#, perl-format
msgid "Possible threats: %d"
msgstr "Talált fertőzések: %d"

#: lib/Scan.pm:363
#, perl-format
msgid "Scanning %s..."
msgstr "%s ellenőrzése..."

#: lib/Scan.pm:448
msgid "None"
msgstr "Nincs"

#: lib/Scan.pm:474
msgid "Cleaning up..."
msgstr "Tisztítás…"

#: lib/Scan.pm:483
msgid "Scanning complete"
msgstr "A vizsgálat befejeződött"

#: lib/Scan.pm:485
msgid "Possible threats found"
msgstr "Lehetséges fertőzések azonosítva"

#: lib/Scan.pm:527
msgid "No threats found"
msgstr "Nem található fertőzés"

#: lib/Scan.pm:564
#, perl-format
msgid "ClamAV Signatures: %d\n"
msgstr "ClamAV Vírusadatbázis: %d\n"

#: lib/Scan.pm:566
msgid "Directories Scanned:\n"
msgstr "Ellenőrzött mappák:\n"

#: lib/Scan.pm:571
#, perl-format
msgid ""
"\n"
"Found %d possible %s (%d %s scanned).\n"
"\n"
msgstr ""
"\n"
"%d találat, amiből %s gyanús (%d %s ellenőrizve).\n"
"\n"

#: lib/Scan.pm:573
msgid "threat"
msgstr "fertőzés"

#: lib/Scan.pm:573
msgid "threats"
msgstr "fertőzés"

#: lib/Scan.pm:575
msgid "file"
msgstr "fájl"

#: lib/Scan.pm:575
msgid "files"
msgstr "fájl"

#: lib/Scan.pm:587
msgid "No threats found.\n"
msgstr "Nem található fenyegető elem.\n"

#: lib/Schedule.pm:46
msgid "Schedule"
msgstr "Ütemezés"

#: lib/Schedule.pm:63
msgid "Scan"
msgstr "Vizsgálat"

#: lib/Schedule.pm:69
msgid "Select a time to scan your home directory"
msgstr "Válasszon időpontot a saját mappája ellenőrzéséhez"

#: lib/Schedule.pm:73
msgid "Set the scan time using a 24 hour clock"
msgstr "Idő beállítása 24 órás formátumban (vizsgálathoz)"

#: lib/Schedule.pm:83 lib/Schedule.pm:139
msgid "Set the hour using a 24 hour clock"
msgstr "Idő beállítása 24 órás formátumban"

#: lib/Schedule.pm:84 lib/Schedule.pm:140
msgid "Hour"
msgstr "Óra"

#: lib/Schedule.pm:91 lib/Schedule.pm:146
msgid "Minute"
msgstr "Perc"

#: lib/Schedule.pm:122 lib/Update.pm:77 lib/Update.pm:201 lib/Update.pm:219
msgid "Antivirus signatures"
msgstr "Adatbázis"

#: lib/Schedule.pm:129
msgid "Select a time to update your signatures"
msgstr "Válasszon időpontot az adatbázis frissítéséhez"

#: lib/Schedule.pm:196 lib/Schedule.pm:258
msgid "A daily scan is scheduled"
msgstr "Napi vizsgálat ütemezve"

#: lib/Schedule.pm:201 lib/Schedule.pm:271
msgid "A daily definitions update is scheduled"
msgstr "Napi adatbázis-frissítés ütemezve"

#: lib/Schedule.pm:262
msgid "A daily scan is not scheduled"
msgstr "Napi vizsgálat nincs ütemezve"

#: lib/Schedule.pm:276
msgid "A daily definitions update is not scheduled"
msgstr "Napi adatbázis-frissítés nincs ütemezve"

#: lib/Schedule.pm:285
msgid "You are set to automatically receive updates"
msgstr "Az adatbázis-frissítés automatikusra van állítva"

#: lib/Settings.pm:42
msgid "Scan for PUAs"
msgstr "Potenciálisan nemkívánatos alkalmazások keresése"

#: lib/Settings.pm:45
msgid "Detect packed binaries, password recovery tools, and more"
msgstr ""
"Becsomagolt futtatható fájlok, jelszó-visszaállító eszközök és hasonlók "
"észlelése"

#: lib/Settings.pm:58
msgid "Scan files beginning with a dot (.*)"
msgstr "A ponttal (.*) kezdődő fájlok ellenőrzése"

#: lib/Settings.pm:60
msgid "Scan files typically hidden from view"
msgstr "Rejtett fájlok vizsgálata"

#: lib/Settings.pm:73
msgid "Scan files larger than 20 MB"
msgstr "20 MB-nál nagyobb fájlok ellenőrzése"

#: lib/Settings.pm:76
msgid "Scan large files which are typically not examined"
msgstr "Nagy fájlok vizsgálata, amelyek általában nincsenek megvizsgálva"

#: lib/Settings.pm:89
msgid "Scan directories recursively"
msgstr "Mappák rekurzív vizsgálata"

#: lib/Settings.pm:92
msgid "Scan all files and directories within a directory"
msgstr "Minden fájl és könyvtár ellenőrzése egy könyvtáron belül"

#: lib/Settings.pm:105
msgid "Check for updates to this program"
msgstr "Programfrissítések keresése"

#: lib/Settings.pm:108
msgid "Check online for application and signature updates"
msgstr "Alkalmazás- és adatbázis-frissítések keresése online"

#: lib/Settings.pm:121
msgid "Double click icons to activate"
msgstr "Dupla kattintás az ikonokra az aktiválásukhoz"

#: lib/Settings.pm:124
msgid "Uncheck this box to activate icons with single click"
msgstr "Törölje, ha egy kattintással szeretné az ikonokat aktiválni"

#: lib/Shortcuts.pm:34
msgid "Exit"
msgstr "Kilépés"

#: lib/Update.pm:57
msgid "Product"
msgstr "Termék"

#: lib/Update.pm:65
msgid "Installed"
msgstr "Telepítve"

#: lib/Update.pm:100
msgid "You are configured to automatically receive updates"
msgstr "Automatikus frissítés beállítva"

#: lib/Update.pm:102
msgid "Check for updates"
msgstr "Frissítések keresése"

#: lib/Update.pm:193
msgid "Downloading..."
msgstr "Letöltés..."

#: lib/Update.pm:223
msgid "Complete"
msgstr "Kész"

#: lib/Whitelist.pm:48
msgid "Directory"
msgstr "Mappa"

#: lib/Whitelist.pm:71
msgid "Add a directory"
msgstr "Könyvtár hozzáadása"

#: lib/Whitelist.pm:82
msgid "Remove a directory"
msgstr "Könyvtár eltávolítása"

#: clamtk-gnome.py:59
#, python-format
msgid "Scan %s for threats..."
msgstr "Fertőzések keresése itt: %s..."

#: clamtk-gnome.py:70
msgid "Scan directory for threats..."
msgstr "Fertőzések keresése a könyvtárban..."

#: clamtk-gnome.py:71
msgid "Scan this directory for threats..."
msgstr "Fertőzések keresése ebben a könyvtárban..."
