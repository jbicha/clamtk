��    &      L  5   |      P  (   Q     z     �     �     �     �     �  	   �     �     �     �     �                      
   6     A     I     P  1   X  $   �     �     �     �     �     �     �            
          	   *     4     9     ?     F  �  N  A   �  -   -  $   [  @   �  !   �     �  ;   �     2  L   N  	   �     �     �     �     �     �  7   	     <	     O	     e	     {	  �   �	  P   
  M   m
  !   �
     �
     �
          (     D     c     p  9   �  0   �     �               0           %                                    
                 #                           "                             !                                       &   	            $    
Found %d possible %s (%d %s scanned).

 Action Taken Analysis Check for updates ClamAV Signatures: %d
 Date Directories Scanned:
 Directory Environment settings Exit File History Never None Please wait... Problems opening %s... Quarantine Restore Result Results Scan all files and directories within a directory Scan files beginning with a dot (.*) Scan files larger than 20 MB Scanning %s... Select a file Set manually Status Unknown Vendor View Viewing %s Virus Scanner Whitelist file files threat threats Project-Id-Version: clamtk
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2015-05-03 04:41-0500
PO-Revision-Date: 2015-07-19 06:46+0000
Last-Translator: Rockworld <sumoisrock@gmail.com>
Language-Team: Thai <th@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2015-09-06 08:00+0000
X-Generator: Launchpad (build 17706)
 
พบ %d อาจเป็น %s (%d %s ตรวจพบ).

 เลือกปฎิบัติการ การวิเคราะห์ โปรแกรม/ข้อมูลรุ่นใหม่ ลายเซ็น ClamAV: %d
 วันที่ โฟลเดอร์ที่ตรวจแล้ว:
 ไดเรกทอรี กำหนด ข้อมูลประกอบการทำงาน ออก แฟ้ม ประวัติ ไม่เคย ไม่มี กรุณารอ... พบปัญหา ในการเปิด %s... กักกัน คืนสภาพ ผลลัพธ์ ผลลัพธ์ ตรวจไวรัสในโฟลเดอร์ที่ระบุ (แฟ้มและโฟลเดอร์ย่อย) ตรวจทุกแฟ้มที่ขึ้นต้นด้วย (.*) ตรวจไวรัสทุกแฟ้มที่ขนาด > 20 MB กำลังตรวจ %s... เลือกแฟ้ม กำหนดเอง สถานภาพ ไม่รู้จัก ผู้จำหน่าย แสดง กำลังแสดง %s โปรแกรมตรวจจับไวรัส บัญชีขาว (ปลอดภัย) ไฟล์ แฟ้ม การคุกคาม การคุกคาม 