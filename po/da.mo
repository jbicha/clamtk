��    K      t  e   �      `  (   a     �     �     �     �     �     �     �     �     �     �          	       9        R  	   h     r     �     �     �     �     �     �     �     �     �  	   �     �     �     �     �     �                    +     0     =     T  
   \     g     l     t     {     �     �     �  1   �     �  $   �     	     6	  1   J	     |	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     
  
   
     %
  	   3
     =
     B
     H
     O
  �  W
  &   �            	        &     .     F     ]     j     n     v     �     �     �  N   �     �     �  	   �                    &     *     =     D  
   M     X     ]     i     o     x          �     �     �     �     �     �     �     �  
   �          	       
        %     4     C  )   H     r  ,   �     �     �  ,   �          '     <     K     X     e     s     �     �     �     �     �     �     �     �     �     �     �     �     �     0          '   G          .      =                     H                              +   !   $   F         (   E   B                 D          1   I      &   2       ;       %   5   	   4   J   A          )   8   7          ?         3   *   9       :   "   -                   <   C               >   ,   
                        /   #      @                6   K       
Found %d possible %s (%d %s scanned).

 About Action Taken Advanced Analysis Check for updates ClamAV Signatures: %d
 Cleaning up... Close Complete Configuration Date Delete Deleted Detect packed binaries, password recovery tools, and more Directories Scanned:
 Directory Downloading... Environment settings Error Exit File File has been saved Help History Homepage Hour Installed Minute Network Never No proxy No threats found.
 None Number Please wait... Port Preparing... Problems opening %s... Product Quarantine Quit Restore Result Results Save file as... Save results Scan Scan all files and directories within a directory Scan directories recursively Scan files beginning with a dot (.*) Scan files larger than 20 MB Scan for threats... Scan large files which are typically not examined Scanning %s... Scanning complete Select a directory Select a file Set manually Settings Settings saved Status Unable to save file Unknown Updates Vendor View Viewing %s Virus Scanner Whitelist file files threat threats Project-Id-Version: clamtk 5.21
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-05-03 04:41-0500
PO-Revision-Date: 2015-10-12 17:26+0000
Last-Translator: Aputsiaĸ Niels Janussen <aj@isit.gl>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2016-08-21 09:40+0000
X-Generator: Launchpad (build 18176)
 
Fandt %d mulige %s (%d %s skannet).

 Om Handling Avanceret Analyse Søg efter opdateringer ClamAV-signaturer: %d
 Rydder op... Luk Færdig Konfiguration Dato Slet Slettet Find pakkede binærfiler, værktøjer til genskabelse af adgangskoder med mere Mapper skannet:
 Mappe Henter... Miljøindstillinger Fejl Afslut Fil Fil er blevet gemt Hjælp Historik Hjemmeside Time Installeret Minut Netværk Aldrig Ingen proxy Ingen trusler fundet
 Ingen Nummer Vent venligst... Port Forbereder... Problem ved åbning af %s... Produkt Karantæne Afslut Genskab Resultat Resultater Gem fil som... Gem resultater Skan Skan alle filer og undermapper i en mappe Skan kataloger rekursivt Skan filer, der begynder med et punktum (.*) Skan filer større end 20 MB Scan efter trusler ... Skan store filer som typisk ikke undersøges Skanner %s... Scanning gennemført Vælg en mappe Vælg en fil Sæt manuelt Indstillinger Indstillinger gemt Status Kunne ikke gemme fil Ukendt Opdateringer Leverandør Vis Viser %s Virusskanner Undtagelser fil filer trussel trusler 