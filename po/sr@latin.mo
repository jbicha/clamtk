��    G      T  a   �                           7  $   @     e     z     �     �     �     �     �     �     �       )        I     N     V  (   _     �  *   �     �     �  O   �     *     F  ;   O  )   �     �     �     �  
   �     	     	     	     	     &	     3	     D	     P	  #   d	  	   �	     �	     �	     �	     �	     �	     �	  (   �	     (
  &   <
     c
     k
     r
     �
     �
     �
     �
     �
     �
     �
             !   2     T  
   h     s  	   �     �  �  �  
   P     [     d     |  2   �     �     �     �     �     �          &  0   E     v     �  (   �     �     �     �  -   �  #     &   :     a     h  R   n  !   �     �  :   �  2   +     ^  '   r     �     �     �     �  	   �     �     �               .  ,   D     q     x     �     �     �     �     �      �       :   '  	   b     l     x     �     �     �     �     �     �  $      $   %  *   J  +   u     �     �     �     �     �             
              <       "       B      !   C   F      )   7   9   ?                  +         '   *             8   :             2   E   (   A      4   	   $          /   =   3             0   >   &   5              .           @   #       1             ,                              -   G              6                 %   ;       D             About Advanced An update is available Analysis Check or recheck a file's reputation ClamTk Virus Scanner Configuration Date Delete file results Edit proxy settings Environment settings Error opening file Error updating: try again later File Analysis File has been saved File successfully submitted for analysis. Help History Homepage I would like to update signatures myself Manage quarantined files My computer automatically receives updates Network Never No information exists for this file. Press OK to submit this file for analysis. No information on this file No proxy Please choose how you will update your antivirus signatures Please install yelp to view documentation Please wait... Press Apply to save changes Problems opening %s... Quarantine Quit Result Results Save file as... Save results Scan a directory Scan a file Scan for threats... Schedule a scan or signature update Scheduler Select a directory Select a file Set manually Settings Signature update preferences Submit file for analysis The antivirus signatures are out-of-date Unable to save file Unable to submit file: try again later Unknown Update Update Assistant Update antivirus signatures Updates Updates are available Vendor View View a file's reputation View and set your preferences View file results View or delete previous results View or update scanning whitelist View previous scans Viewing %s Virus Scanner Whitelist Your changes were saved. Project-Id-Version: clamtk
Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>
POT-Creation-Date: 2014-01-10 07:13-0600
PO-Revision-Date: 2014-01-13 14:52+0000
Last-Translator: Moki Ding <moki.forum@gmail.com>
Language-Team: Serbian Latin <sr@latin@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2014-01-13 22:44+0000
X-Generator: Launchpad (build 16890)
 O Programu Napredno Nadogradnja je dostupna Analiza Proverite ili ponovo proverite reputaciju datoteke ClamTk Virus Skener Podešavanje Datum Obriši rezultate datoteke Izmeni proxy postavke Podešavanje ambijenta Greška pri otvaranju datoteke Greška u nadogradnji: pokušajte ponovo kasnije Analiza datoteke Datoteka je sačuvana Datoteka uspešno postavljena za analizu Pomoć Istorija Početna stranica Voleo bih da nadograđujem potpise samostalno Upravljanje datotekama iz karantina Moj računar automatski prima ispravke Mreža Nikad Nema informacija za ovu datoteku. Pritisnite OK da pošaljete datoteku na analizu. Nema informacija na ovoj datoteci Bez proksija Molimo izaberite kako ćete ažurirati antivirusne potpise Molimo instalirajte "yelp" da vidite ovaj dokument Molim sačekajte... Pritisnite Potvrdi da sačuvate promene Problem sa otvaranjem %s... Karantin Izađi Rezultat Rezultati Sačuvajte datoteku kao... Sačuvaj rezultat Skeniraj direktorijum Skeniraj datoteku Skeniranje pretnji... Zakažite skeniranje ili nadogradnju potpisa Planer Izaberite direktorijum Izaberite datoteku Postavi ručno Podešavanja Podešavanje Pošaljite datoteku na analizu Antivirusni potpisi su zastareli Nemoguće je sačuvati fajl Nemoguće je postaviti datoteku: pokušajte ponovo kasnije Nepoznato Nadogradnja Asistent nadogradnje Nadogradi antivirusne potpise Nadogradnja Nadogradnje su dostupne Izdavač Pogledaj Pogledajte reputaciju datoteke Pogledajte i postavite vase postavke Pogledajte rezultate za ovu datoteku Pogledajte ili obriši prethodne rezultate Pogledaj ili nadogradi skeniranje izuzetaka Pogledajte prethodna skeniranja Pregledate %s Virus Skener Izuzeci Tvoje promene su sačuvane 