# English (United Kingdom) translation for clamtk
# Copyright (c) Dave M <dave.nerd@gmail>, 2004-2020.
# This file is distributed under the same license as the clamtk package.
# Jen Ockwell, Anthony Scarth, Ken Sharp, Will Bickerstaff, ezekiel000,
# maty, Max Eaves, Vibhav Pant, Anthony Harrington, fossfreedom,
# Andi Chandler
#
msgid ""
msgstr ""
"Project-Id-Version: clamtk\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2020-08-05 05:26-0500\n"
"PO-Revision-Date: 2020-08-07 08:30+0000\n"
"Last-Translator: Anthony Harrington <Unknown>\n"
"Language-Team: English (United Kingdom) <en_GB@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2020-08-07 09:48+0000\n"
"X-Generator: Launchpad (build a24057fea7e4c6a98c0220d5f878da0f3c783699)\n"
"Language: English (en_GB)\n"

#: lib/Analysis.pm:66 lib/GUI.pm:456 lib/GUI.pm:536 lib/GUI.pm:677
#: lib/Results.pm:156 lib/Results.pm:231
msgid "Analysis"
msgstr "Analysis"

#: lib/Analysis.pm:78 lib/Schedule.pm:66
msgid "ClamTk Virus Scanner"
msgstr "ClamTk Virus Scanner"

#: lib/Analysis.pm:83
msgid ""
"This section allows you to check the status of a file from Virustotal. If a "
"file has been previously submitted, you will see the results of the scans "
"from dozens of antivirus and security vendors. This allows you to make a "
"more informed decision about the safety of a file.\n"
"\n"
"If the file has never been submitted to Virustotal, you can submit it "
"yourself. When you resubmit the file minutes later, you should see the "
"results.\n"
"\n"
"Please note you should never submit sensitive files to Virustotal.  Any "
"uploaded file can be viewed by Virustotal customers. Do not submit anything "
"with personal information or passwords.\n"
"\n"
"https://www.virustotal.com/gui/home/search"
msgstr ""
"This section allows you to check the status of a file from Virustotal. If a "
"file has been previously submitted, you will see the results of the scans "
"from dozens of antivirus and security vendors. This allows you to make a "
"more informed decision about the safety of a file.\n"
"\n"
"If the file has never been submitted to Virustotal, you can submit it "
"yourself. When you resubmit the file minutes later, you should see the "
"results.\n"
"\n"
"Please note you should never submit sensitive files to Virustotal.  Any "
"uploaded file can be viewed by Virustotal customers. Do not submit anything "
"with personal information or passwords.\n"
"\n"
"https://www.virustotal.com/gui/home/search"

#: lib/Analysis.pm:93
msgid "Help"
msgstr "Help"

#: lib/Analysis.pm:96
msgid "What is this?"
msgstr "What is this?"

#: lib/Analysis.pm:111
msgid "File Analysis"
msgstr "File Analysis"

#: lib/Analysis.pm:118 lib/Results.pm:36 lib/Results.pm:41
msgid "Results"
msgstr "Results"

#: lib/Analysis.pm:127 lib/History.pm:209 lib/Results.pm:169
#: lib/Schedule.pm:193 lib/Schedule.pm:242
msgid "Close"
msgstr "Close"

#: lib/Analysis.pm:184
msgid "Vendor"
msgstr "Vendor"

#: lib/Analysis.pm:193
msgid "Date"
msgstr "Date"

#: lib/Analysis.pm:202
msgid "Result"
msgstr "Result"

#: lib/Analysis.pm:222
msgid "Save results"
msgstr "Save results"

#: lib/Analysis.pm:240
msgid "Check or recheck a file's reputation"
msgstr "Check or recheck a file's reputation"

#: lib/Analysis.pm:259 lib/GUI.pm:576 lib/Shortcuts.pm:41
msgid "Select a file"
msgstr "Select a file"

#: lib/Analysis.pm:281
msgid "Submit file for analysis"
msgstr "Submit file for analysis"

#: lib/Analysis.pm:296
msgid "Uploaded files must be smaller than 32MB"
msgstr "Uploaded files must be smaller than 32MB"

#: lib/Analysis.pm:312
msgid ""
"No information exists for this file. Press OK to submit this file for "
"analysis."
msgstr ""
"No information exists for this file. Press OK to submit this file for "
"analysis."

#: lib/Analysis.pm:323 lib/Analysis.pm:639
msgid "Unable to submit file: try again later"
msgstr "Unable to submit file: try again later"

#: lib/Analysis.pm:336
msgid "View or delete previous results"
msgstr "View or delete previous results"

#: lib/Analysis.pm:363
msgid "View file results"
msgstr "View file results"

#: lib/Analysis.pm:428
msgid "Delete file results"
msgstr "Delete file results"

#: lib/Analysis.pm:501 lib/Analysis.pm:595 lib/Assistant.pm:100 lib/Scan.pm:102
#: lib/Update.pm:154
msgid "Please wait..."
msgstr "Please wait..."

#: lib/Analysis.pm:637
msgid "File successfully submitted for analysis."
msgstr "File successfully submitted for analysis."

#: lib/Analysis.pm:697 lib/Analysis.pm:702
msgid "Unable to save file"
msgstr "Unable to save file"

#: lib/Analysis.pm:711
msgid "File has been saved"
msgstr "File has been saved"

#: lib/App.pm:246
msgid "Unknown"
msgstr "Unknown"

#: lib/App.pm:397 lib/Prefs.pm:176
msgid "Never"
msgstr "Never"

#: lib/App.pm:455
msgid "Scan for threats..."
msgstr "Scan for threats..."

#: lib/App.pm:456
msgid "Advanced"
msgstr "Advanced"

#: lib/App.pm:457 lib/GUI.pm:430 lib/GUI.pm:431 lib/GUI.pm:539
msgid "Scan a file"
msgstr "Scan a file"

#: lib/App.pm:458 lib/GUI.pm:435 lib/GUI.pm:436 lib/GUI.pm:542
msgid "Scan a directory"
msgstr "Scan a directory"

#: lib/App.pm:460 lib/GUI.pm:718
msgid "ClamTk is a graphical front-end for Clam Antivirus"
msgstr "ClamTk is a graphical front-end for Clam Antivirus"

#: lib/Assistant.pm:38
msgid "Please choose how you will update your antivirus signatures"
msgstr "Please choose how you will update your antivirus signatures"

#: lib/Assistant.pm:54
msgid "My computer automatically receives updates"
msgstr "My computer automatically receives updates"

#: lib/Assistant.pm:68
msgid "I would like to update signatures myself"
msgstr "I would like to update signatures myself"

#: lib/Assistant.pm:93 lib/Assistant.pm:150 lib/Network.pm:110
msgid "Press Apply to save changes"
msgstr "Press Apply to save changes"

#: lib/Assistant.pm:122
msgid "Your changes were saved."
msgstr "Your changes were saved."

#: lib/Assistant.pm:125 lib/Update.pm:180 lib/Update.pm:186
msgid "Error updating: try again later"
msgstr "Error updating: try again later"

#: lib/GUI.pm:69
msgid "Virus Scanner"
msgstr "Virus Scanner"

#: lib/GUI.pm:81 lib/GUI.pm:533 lib/Shortcuts.pm:29
msgid "About"
msgstr "About"

#: lib/GUI.pm:127
msgid "Updates are available"
msgstr "Updates are available"

#: lib/GUI.pm:130
msgid "The antivirus signatures are outdated"
msgstr "The antivirus signatures are outdated"

#: lib/GUI.pm:133
msgid "An update is available"
msgstr "An update is available"

#: lib/GUI.pm:217 lib/GUI.pm:555
msgid "Settings"
msgstr "Settings"

#: lib/GUI.pm:218
msgid "View and set your preferences"
msgstr "View and set your preferences"

#: lib/GUI.pm:222 lib/GUI.pm:563
msgid "Whitelist"
msgstr "Whitelist"

#: lib/GUI.pm:223
msgid "View or update scanning whitelist"
msgstr "View or update scanning whitelist"

#: lib/GUI.pm:227 lib/GUI.pm:561
msgid "Network"
msgstr "Network"

#: lib/GUI.pm:228
msgid "Edit proxy settings"
msgstr "Edit proxy settings"

#: lib/GUI.pm:232 lib/GUI.pm:545 lib/Schedule.pm:54
msgid "Scheduler"
msgstr "Scheduler"

#: lib/GUI.pm:233
msgid "Schedule a scan or signature update"
msgstr "Schedule a scan or signature update"

#: lib/GUI.pm:295 lib/GUI.pm:557
msgid "Update"
msgstr "Update"

#: lib/GUI.pm:296
msgid "Update antivirus signatures"
msgstr "Update antivirus signatures"

#: lib/GUI.pm:300 lib/GUI.pm:565
msgid "Update Assistant"
msgstr "Update Assistant"

#: lib/GUI.pm:301
msgid "Signature update preferences"
msgstr "Signature update preferences"

#: lib/GUI.pm:363 lib/GUI.pm:553 lib/GUI.pm:665 lib/History.pm:41
msgid "History"
msgstr "History"

#: lib/GUI.pm:364
msgid "View previous scans"
msgstr "View previous scans"

#: lib/GUI.pm:368 lib/GUI.pm:559 lib/Results.pm:139 lib/Results.pm:216
msgid "Quarantine"
msgstr "Quarantine"

#: lib/GUI.pm:369
msgid "Manage quarantined files"
msgstr "Manage quarantined files"

#: lib/GUI.pm:457
msgid "View a file's reputation"
msgstr "View a file's reputation"

#: lib/GUI.pm:599 lib/GUI.pm:637 lib/Scan.pm:777 lib/Scan.pm:788
msgid "You do not have permissions to scan that file or directory"
msgstr "You do not have permissions to scan that file or directory"

#: lib/GUI.pm:613 lib/Shortcuts.pm:47 lib/Whitelist.pm:100
msgid "Select a directory"
msgstr "Select a directory"

#: lib/GUI.pm:658
msgid "Configuration"
msgstr "Configuration"

#: lib/GUI.pm:671
msgid "Updates"
msgstr "Updates"

#: lib/GUI.pm:709
msgid "Homepage"
msgstr "Homepage"

#: lib/History.pm:80
msgid "View"
msgstr "View"

#: lib/History.pm:88
msgid "Print"
msgstr "Print"

#: lib/History.pm:105 lib/Quarantine.pm:98 lib/Results.pm:146
#: lib/Results.pm:224 lib/Schedule.pm:203
msgid "Delete"
msgstr "Delete"

#: lib/History.pm:150
#, perl-format
msgid "Viewing %s"
msgstr "Viewing %s"

#: lib/History.pm:166
#, perl-format
msgid "Problems opening %s..."
msgstr "Problems opening %s..."

#: lib/Network.pm:41
msgid "No proxy"
msgstr "No proxy"

#: lib/Network.pm:46
msgid "Environment settings"
msgstr "Environment settings"

#: lib/Network.pm:51
msgid "Set manually"
msgstr "Set manually"

#: lib/Network.pm:55
msgid "IP address or host"
msgstr "IP address or host"

#: lib/Network.pm:76
msgid "Port"
msgstr "Port"

#: lib/Network.pm:263
msgid "Settings saved"
msgstr "Settings saved"

#: lib/Network.pm:270
msgid "Error"
msgstr "Error"

#: lib/Quarantine.pm:56
msgid "Number"
msgstr "Number"

#: lib/Quarantine.pm:67 lib/Results.pm:78
msgid "File"
msgstr "File"

#: lib/Quarantine.pm:85
msgid "Restore"
msgstr "Restore"

#: lib/Quarantine.pm:124 lib/Results.pm:281
#, perl-format
msgid "Really delete this file (%s) ?"
msgstr "Really delete this file (%s) ?"

#: lib/Quarantine.pm:185
msgid "Save file as..."
msgstr "Save file as..."

#: lib/Results.pm:89 lib/Schedule.pm:215
msgid "Status"
msgstr "Status"

#: lib/Results.pm:100
msgid "Action Taken"
msgstr "Action Taken"

#: lib/Results.pm:219
msgid "Quarantined"
msgstr "Quarantined"

#: lib/Results.pm:227
msgid "Deleted"
msgstr "Deleted"

#: lib/Scan.pm:141
msgid "Preparing..."
msgstr "Preparing..."

#: lib/Scan.pm:152 lib/Scan.pm:415
#, perl-format
msgid "Files scanned: %d"
msgstr "Files scanned: %d"

#: lib/Scan.pm:156 lib/Scan.pm:486
#, perl-format
msgid "Possible threats: %d"
msgstr "Possible threats: %d"

#: lib/Scan.pm:398
#, perl-format
msgid "Scanning %s..."
msgstr "Scanning %s..."

#: lib/Scan.pm:484
msgid "None"
msgstr "None"

#: lib/Scan.pm:510
msgid "Cleaning up..."
msgstr "Cleaning up..."

#: lib/Scan.pm:517 lib/Update.pm:224 lib/Update.pm:227
msgid "Complete"
msgstr "Complete"

#: lib/Scan.pm:520
msgid "Scanning complete"
msgstr "Scanning complete"

#: lib/Scan.pm:522
msgid "Possible threats found"
msgstr "Possible threats found"

#: lib/Scan.pm:564
msgid "No threats found"
msgstr "No threats found"

#: lib/Scan.pm:601
#, perl-format
msgid "ClamAV Signatures: %d\n"
msgstr "ClamAV Signatures: %d\n"

#: lib/Scan.pm:603
msgid "Directories Scanned:\n"
msgstr "Directories Scanned:\n"

#: lib/Scan.pm:608
#, perl-format
msgid ""
"\n"
"Found %d possible %s (%d %s scanned).\n"
"\n"
msgstr ""
"\n"
"Found %d possible %s (%d %s scanned).\n"
"\n"

#: lib/Scan.pm:610
msgid "threat"
msgstr "threat"

#: lib/Scan.pm:610
msgid "threats"
msgstr "threats"

#: lib/Scan.pm:612
msgid "file"
msgstr "file"

#: lib/Scan.pm:612
msgid "files"
msgstr "files"

#: lib/Scan.pm:624
msgid "No threats found.\n"
msgstr "No threats found.\n"

#: lib/Schedule.pm:81
msgid "Scan"
msgstr "Scan"

#: lib/Schedule.pm:88
msgid "Select a time to scan your home directory"
msgstr "Select a time to scan your home directory"

#: lib/Schedule.pm:92
msgid "Set the scan time using a 24 hour clock"
msgstr "Set the scan time using a 24 hour clock"

#: lib/Schedule.pm:103 lib/Schedule.pm:163
msgid "Set the hour using a 24 hour clock"
msgstr "Set the hour using a 24 hour clock"

#: lib/Schedule.pm:104 lib/Schedule.pm:164
msgid "Hour"
msgstr "Hour"

#: lib/Schedule.pm:111 lib/Schedule.pm:170
msgid "Minute"
msgstr "Minute"

#: lib/Schedule.pm:146 lib/Update.pm:80 lib/Update.pm:205 lib/Update.pm:220
msgid "Antivirus signatures"
msgstr "Antivirus signatures"

#: lib/Schedule.pm:153
msgid "Select a time to update your signatures"
msgstr "Select a time to update your signatures"

#: lib/Schedule.pm:224 lib/Schedule.pm:288
msgid "A daily scan is scheduled"
msgstr "A daily scan is scheduled"

#: lib/Schedule.pm:229 lib/Schedule.pm:301
msgid "A daily definitions update is scheduled"
msgstr "A daily definitions update is scheduled"

#: lib/Schedule.pm:292
msgid "A daily scan is not scheduled"
msgstr "A daily scan is not scheduled"

#: lib/Schedule.pm:306
msgid "A daily definitions update is not scheduled"
msgstr "A daily definitions update is not scheduled"

#: lib/Schedule.pm:315
msgid "You are set to automatically receive updates"
msgstr "You are set to automatically receive updates"

#: lib/Settings.pm:42
msgid "Scan for PUAs"
msgstr "Scan for PUAs"

#: lib/Settings.pm:44
msgid "Detect packed binaries, password recovery tools, and more"
msgstr "Detect packed binaries, password recovery tools, and more"

#: lib/Settings.pm:57
msgid "Use heuristic scanning"
msgstr "Use heuristic scanning"

#: lib/Settings.pm:72
msgid "Scan files beginning with a dot (.*)"
msgstr "Scan files beginning with a dot (.*)"

#: lib/Settings.pm:73
msgid "Scan files typically hidden from view"
msgstr "Scan files typically hidden from view"

#: lib/Settings.pm:86
msgid "Scan files larger than 20 MB"
msgstr "Scan files larger than 20 MB"

#: lib/Settings.pm:88
msgid "Scan large files which are typically not examined"
msgstr "Scan large files which are typically not examined"

#: lib/Settings.pm:101
msgid "Scan directories recursively"
msgstr "Scan directories recursively"

#: lib/Settings.pm:103
msgid "Scan all files and directories within a directory"
msgstr "Scan all files and directories within a directory"

#: lib/Settings.pm:116
msgid "Check for updates to this program"
msgstr "Check for updates to this program"

#: lib/Settings.pm:118
msgid "Check online for application and signature updates"
msgstr "Check online for application and signature updates"

#: lib/Shortcuts.pm:35
msgid "Exit"
msgstr "Exit"

#: lib/Update.pm:60
msgid "Product"
msgstr "Product"

#: lib/Update.pm:68
msgid "Installed"
msgstr "Installed"

#: lib/Update.pm:105
msgid "You are configured to automatically receive updates"
msgstr "You are configured to automatically receive updates"

#: lib/Update.pm:108
msgid "Check for updates"
msgstr "Check for updates"

#: lib/Update.pm:199
msgid "Downloading..."
msgstr "Downloading..."

#: lib/Whitelist.pm:48
msgid "Directory"
msgstr "Directory"

#: lib/Whitelist.pm:70
msgid "Add a directory"
msgstr "Add a directory"

#: lib/Whitelist.pm:83
msgid "Remove a directory"
msgstr "Remove a directory"
